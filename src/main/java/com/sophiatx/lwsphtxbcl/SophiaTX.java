/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.chain.model.BlockId;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.chain.model.PublicKey;
import com.sophiatx.lwsphtxbcl.chain.model.Transaction;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AbstractOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AccountCreateOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AccountWitnessVoteOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.CustomJsonOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.SponsorFeesOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.TransferOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.TransferToVestingOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.WithdrawVestingOperation;
import com.sophiatx.lwsphtxbcl.comm.Connector;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiVersionObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiWitnessScheduleObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.DynamicGlobalProperties;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetDynamicGlobalPropertiesRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetVersionRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetWitnessScheduleRequest;
import com.sophiatx.lwsphtxbcl.comm.api.database.request.GetCurrentPriceFeedRequest;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.comm.exception.ConnectionException;
import com.sophiatx.lwsphtxbcl.crypto.KeyPair;

import java.math.BigDecimal;
import java.util.List;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

public class SophiaTX
{
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private Connector c;
    private ApiVersionObject version;

    /**
     * Instantiate a new SophiaTX helper class.
     *
     * This will automatically connect to the network using the given connector
     * to obtain some information about the chain ( needed for TX assembling ).
     *
     * @param c Connector instance
     */
    public SophiaTX(Connector c)
    {
        verifyNotNull(c);
        this.c = c;

        // obtain version info as well as chain ID ( needed for signatures )
        try {
            this.version = c.execute(new GetVersionRequest());
        } catch (ConnectionException e) {
            throw new RuntimeException("Invalid connector configuration, node is unreachable");
        } catch (CommunicationException e) {
            throw new RuntimeException("Unable to obtain network info", e);
        }
    }

    /**
     * This method will instantiate a new transaction, obtaining the reference
     * block data from the network
     *
     * @return Transaction
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public Transaction createTransaction()
    throws CommunicationException
    {
        return new Transaction(getHeadBlockId());
    }

    /**
     * This method will obtain and return the current head block ID
     *
     * @return Current head block ID
     * @throws CommunicationException There was an error pulling the data
     */
    public BlockId getHeadBlockId()
    throws CommunicationException
    {
        return getDGP().getHeadBlockId();
    }

    /**
     * This method will obtain and return the current dynamic global properties
     *
     * @return Current dynamic global properties
     * @throws CommunicationException There was an error pulling the data
     */
    public DynamicGlobalProperties getDGP()
    throws CommunicationException
    {
        return c.execute(new GetDynamicGlobalPropertiesRequest());
    }

    /**
     * This method will obtain and return the current witness schedule
     *
     * @return Current witness schedule
     * @throws CommunicationException There was an error pulling the data
     */
    public ApiWitnessScheduleObject getWitnessSchedule()
    throws CommunicationException
    {
        return c.execute(new GetWitnessScheduleRequest());
    }

    /**
     * This method will obtain and return the current SPHTX-USD conversion rate
     *
     * @return Conversion rate between SPHTX and USD
     * @throws CommunicationException There was an error pulling the data
     */
    public ConversionRate getConversionRateUSD()
    throws CommunicationException
    {
        return c.execute(new GetCurrentPriceFeedRequest(AssetSymbol.USD));
    }

    /**
     * Transfer tokens to another account
     *
     * @param from   Name of the account to send the funds from
     * @param to     Beneficiary account name
     * @param amount Amount to transfer
     * @param memo   Notes for the receiver
     * @param kp     Key pair used to sign the transaction
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String transfer(String from, String to, BigDecimal amount, String memo, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new TransferOperation(from, to, new Asset(amount, AssetSymbol.SPHTX), memo),
                kp
        );
    }

    /**
     * Transfer tokens to the origin's or another account's vesting balance.
     * Set second argument to null if transferring to origin account.
     *
     * @param from   Name of the account to send the funds from
     * @param to     Beneficiary account name
     * @param amount Amount to transfer to recipient's vesting balance
     * @param kp     Key pair used to sign the transaction
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String transferToVesting(String from, String to, BigDecimal amount, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new TransferToVestingOperation(from, to, new Asset(amount, AssetSymbol.SPHTX)),
                kp
        );
    }

    /**
     * Withdraw tokens from vesting
     *
     * @param account Name of the accounts the vesting will be withdrawn
     * @param amount  Amount to withdraw
     * @param kp      Key pair used to sign the transaction
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String withdrawVesting(String account, BigDecimal amount, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new WithdrawVestingOperation(account, new Asset(amount, AssetSymbol.VESTS)),
                kp
        );
    }

    /**
     * Vote for a witness
     *
     * @param votingAccount Account to cast the vote from
     * @param witness       Witness to vote for
     * @param approve       Whether to vote for the witness ( false = remove vote )
     * @param kp            Key pair used to sign the transaction
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String vote(String votingAccount, String witness, boolean approve, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new AccountWitnessVoteOperation(votingAccount, witness, approve), kp);
    }

    /**
     * Sponsor fees of a certain account
     *
     * @param sponsor      Account that will pay for the fees
     * @param sponsored    Account that will have the fees sponsored
     * @param isSponsoring Whether the sponsoring is active
     * @param kp           Key pair used to sign the transaction
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String sponsorFees(String sponsor, String sponsored, boolean isSponsoring, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new SponsorFeesOperation(sponsor, sponsored, isSponsoring), kp);
    }

    /**
     * Send a JSON message to the given list of recipients
     *
     * @param sender     Account to send the message from
     * @param recipients Recipients
     * @param appId      ID of application the message is associated with
     * @param json       JSON to send
     * @param kp         Key Pair
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String sendJson(String sender, List<String> recipients, long appId, String json, KeyPair kp)
    throws CommunicationException
    {
        return executeOperation(
                new CustomJsonOperation(sender, recipients, appId, json), kp);
    }

    /**
     * Create a new account on the blockchain.
     *
     * @param creator      An account that will pay for the account creation
     * @param nameSeed     Seed that will be used to generate the username
     * @param owner        Owner authority
     * @param active       Active authority
     * @param memoKey      Memo key
     * @param jsonMetadata JSON metadata
     * @return Transaction ID
     * @throws CommunicationException There was an error broadcasting the TX
     */
    public String createAccount(String creator, String nameSeed, Authority owner,
                                Authority active, PublicKey memoKey,
                                String jsonMetadata, KeyPair kp)
    throws CommunicationException
    {
        // create account operation has an exception in the fee system, instead
        // of it being calculated, it must be obtained from the witness schedule
        // object which contains the current fee agreed on by the witnesses
        Asset fee = getWitnessSchedule()
                .getMedianProps()
                .getAccountCreationFee();

        return createTransaction()
                .addOperation(new AccountCreateOperation(fee, creator, nameSeed, owner, active, memoKey, jsonMetadata))
                .sign(kp)
                .broadcast(c)
                .getId();
    }

    /**
     * Calculate fees, sign & broadcast the transaction, returning TX ID
     *
     * @return Transaction ID
     */
    private String executeOperation(AbstractOperation operation, KeyPair kp)
    throws CommunicationException
    {
        return createTransaction()
                .addOperation(operation)
                .calculateFees(getConversionRateUSD())
                .sign(kp, version.getChainId())
                .broadcast(c)
                .getId();
    }

    public Connector getConnector()
    {
        return c;
    }
}
