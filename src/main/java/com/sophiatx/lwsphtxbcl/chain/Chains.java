/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain;

public class Chains
{
    public static final String MAIN_NET  = "1a058d1a89aff240ab203abe8a429d1a1699c339032a87e70e01022842a98324";
    public static final String DEV_NET   = "0c7a75781ccdc99239df3e374fa1bfd6f355ad46485c8bcbb836db3a5b909971";
    public static final String STAGE_NET = "e2eb985225fdef86035300816fb3d81f4559eb7a2f890ec0fbc2057beb91ec4b";
}
