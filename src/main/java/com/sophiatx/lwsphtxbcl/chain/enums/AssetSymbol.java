/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.enums;

import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.util.HashMap;
import java.util.Map;

public enum AssetSymbol implements BinarySerializable
{
    VESTS,
    SPHTX,
    USD,
    EUR,
    CHF,
    CNY,
    GBP;

    private static Map<String, AssetSymbol> mappings = new HashMap<>();

    static {
        for (AssetSymbol s : AssetSymbol.class.getEnumConstants()) {
            mappings.put(s.name().toUpperCase(), s);
        }
    }

    public static AssetSymbol forName(String value)
    {
        return mappings.getOrDefault(value.toUpperCase(), null);
    }

    @Override
    public byte[] toBytes()
    {
        byte[] symbol = BinUtils.toBytes(super.name());
        byte[] buffer = new byte[8];
        System.arraycopy(symbol, 0, buffer, 0, symbol.length);
        return buffer;
    }
}
