/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.enums;

import java.util.HashMap;
import java.util.Map;

public enum OperationType
{
    TRANSFER,
    TRANSFER_TO_VESTING,
    WITHDRAW_VESTING,
    FEED_PUBLISH,
    ACCOUNT_CREATE,
    ACCOUNT_UPDATE,
    ACCOUNT_DELETE,
    WITNESS_UPDATE,
    WITNESS_STOP,
    ACCOUNT_WITNESS_VOTE,
    ACCOUNT_WITNESS_PROXY,
    WITNESS_SET_PROPERTIES,
    CUSTOM, // removed in favour of custom_json and custom_binary
    CUSTOM_JSON,
    CUSTOM_BINARY,
    REQUEST_ACCOUNT_RECOVERY,
    RECOVER_ACCOUNT,
    CHANGE_RECOVERY_ACCOUNT,
    ESCROW_TRANSFER,
    ESCROW_DISPUTE,
    ESCROW_RELEASE,
    ESCROW_APPROVE,
    RESET_ACCOUNT,
    SET_RESET_ACCOUNT,
    APPLICATION_CREATE,
    APPLICATION_UPDATE,
    APPLICATION_DELETE,
    BUY_APPLICATION,
    CANCEL_APPLICATION_BUYING,
    TRANSFER_FROM_PROMOTION_POOL,
    SPONSOR_FEES,
    INTEREST,
    FILL_VESTING_WITHDRAW,
    SHUTDOWN_WITNESS,
    HARDFORK,
    PRODUCER_REWARD,
    PROMOTION_POOL_WITHDRAW;

    private static Map<String, OperationType> mappings = new HashMap<>();

    static {
        for (OperationType t : OperationType.class.getEnumConstants()) {
            mappings.put(t.name().toLowerCase(), t);
        }
    }

    public static OperationType forName(String value)
    {
        return mappings.getOrDefault(value.toLowerCase(), null);
    }
}
