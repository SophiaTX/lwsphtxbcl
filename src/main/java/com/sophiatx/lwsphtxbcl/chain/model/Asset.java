/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonIgnoreProperties({"amount", "symbol"})
public class Asset implements BinarySerializable
{
    private static final Integer PRECISION = 6;
    private static final Pattern PATTERN
            = Pattern.compile("^([0-9]+(?:\\.[0-9]{6})?) ([A-Z]{3,6})$");

    public static final Asset ZERO_SPHTX = new Asset("0", AssetSymbol.SPHTX);

    private BigDecimal amount;
    private AssetSymbol symbol;

    public Asset(String amount, AssetSymbol symbol)
    {
        setAmount(new BigDecimal(amount));
        this.symbol = symbol;
    }

    public Asset(BigDecimal amount, AssetSymbol symbol)
    {
        setAmount(amount);
        this.symbol = symbol;
    }

    private void setAmount(BigDecimal amount)
    {
        this.amount = amount.setScale(PRECISION, BigDecimal.ROUND_HALF_UP);
    }

    @JsonCreator
    public Asset(String assetString)
    {
        Matcher matcher = PATTERN.matcher(assetString);
        verifyPrecondition(matcher.find(), "Invalid asset format");
        String amt = matcher.group(1);
        String sym = matcher.group(2);

        AssetSymbol symbol = AssetSymbol.forName(sym);
        verifyPrecondition(symbol != null, "Invalid asset symbol");

        setAmount(new BigDecimal(amt));
        this.symbol = symbol;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public AssetSymbol getSymbol()
    {
        return symbol;
    }

    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(BinUtils.toBytes(amount.movePointRight(6).longValueExact()));
        bytes.append(BinUtils.toBytes(symbol));
        return bytes.getBytes();
    }

    @Override
    @JsonValue
    public String toString()
    {
        return amount.toPlainString() + " " + symbol.name();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Asset)) return false;
        Asset asset = (Asset) o;
        return Objects.equals(amount, asset.amount) &&
                symbol == asset.symbol;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(amount, symbol);
    }
}
