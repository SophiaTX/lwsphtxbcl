/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.conversion.FlatMapDeserializer;
import com.sophiatx.lwsphtxbcl.utils.conversion.FlatMapSerializer;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.util.Map;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

public class ChainProperties implements BinarySerializable
{
    @JsonProperty("account_creation_fee")
    private Asset accountCreationFee;

    @JsonProperty("maximum_block_size")
    private UInteger maximumBlockSize;

    @JsonProperty("price_feeds")
    @JsonSerialize(using = FlatMapSerializer.class)
    @JsonDeserialize(
            using = FlatMapDeserializer.class,
            keyAs = AssetSymbol.class,
            contentAs = ConversionRate.class
    )
    private Map<AssetSymbol, ConversionRate> priceFeeds;

    @JsonCreator
    public ChainProperties(
            @JsonProperty("account_creation_fee") Asset accountCreationFee,
            @JsonProperty("maximum_block_size") UInteger maximumBlockSize,
            @JsonProperty("price_feeds") Map<AssetSymbol, ConversionRate> priceFeeds
    )
    {
        verifyNotNull(accountCreationFee, maximumBlockSize, priceFeeds);
        this.accountCreationFee = accountCreationFee;
        this.maximumBlockSize = maximumBlockSize;
        this.priceFeeds = priceFeeds;
    }

    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(BinUtils.toBytes(accountCreationFee));
        bytes.append(BinUtils.toBytes(maximumBlockSize));
        bytes.append(BinUtils.toBytes(priceFeeds));
        return bytes.getBytes();
    }

    public Asset getAccountCreationFee()
    {
        return accountCreationFee;
    }

    public UInteger getMaximumBlockSize()
    {
        return maximumBlockSize;
    }

    public Map<AssetSymbol, ConversionRate> getPriceFeeds()
    {
        return priceFeeds;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ChainProperties)) return false;
        ChainProperties that = (ChainProperties) o;
        return Objects.equals(accountCreationFee, that.accountCreationFee) &&
                Objects.equals(maximumBlockSize, that.maximumBlockSize) &&
                Objects.equals(priceFeeds, that.priceFeeds);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(accountCreationFee, maximumBlockSize, priceFeeds);
    }
}
