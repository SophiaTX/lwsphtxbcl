/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

/**
 * Implementation of the "Price" model used on the blockchain
 *
 * I found the "Price" model name to be a bit confusing, so I decided to use
 * the "ConversionRate" model name for this library instead.
 */
@JsonPropertyOrder({"base", "quote"})
public class ConversionRate implements BinarySerializable
{
    @JsonProperty("base")
    private Asset base;
    @JsonProperty("quote")
    private Asset quote;
    @JsonIgnore
    private BigDecimal rate;

    @JsonCreator
    public ConversionRate(
            @JsonProperty("base") Asset base,
            @JsonProperty("quote") Asset quote
    )
    {
        verifyNotNull(base, quote);
        this.base = base;
        this.quote = quote;

        this.rate = quote.getAmount()
                .divide(base.getAmount(), new MathContext(6, RoundingMode.HALF_UP));
    }

    public Asset getBase()
    {
        return base;
    }

    public Asset getQuote()
    {
        return quote;
    }

    public Asset convert(Asset fromAsset)
    {
        verifyNotNull(fromAsset);
        BigDecimal result;
        AssetSymbol resultSymbol;

        if (fromAsset.getSymbol() == base.getSymbol()) {
            // converting from base
            result = fromAsset.getAmount().multiply(rate);
            resultSymbol = quote.getSymbol();
        } else if (fromAsset.getSymbol() == quote.getSymbol()) {
            // converting to base
            result = fromAsset.getAmount().divide(rate, RoundingMode.HALF_UP);
            resultSymbol = base.getSymbol();
        } else {
            throw new IllegalArgumentException("Attempted to convert asset " +
                    fromAsset.getSymbol().toString() + ", can only convert between " +
                    base.getSymbol().toString() + " and " + quote.getSymbol().toString());
        }

        return new Asset(result, resultSymbol);
    }

    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(BinUtils.toBytes(base));
        bytes.append(BinUtils.toBytes(quote));
        return bytes.getBytes();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ConversionRate)) return false;
        ConversionRate that = (ConversionRate) o;
        return Objects.equals(base, that.base) &&
                Objects.equals(quote, that.quote);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(base, quote);
    }
}
