/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

// not yet implemented on the blockchain side
public class FutureExtension implements BinarySerializable
{
    @Override
    public byte[] toBytes()
    {
        return new byte[0];
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        return o instanceof FutureExtension;
    }

    public int hashCode()
    {
        return 1;
    }
}
