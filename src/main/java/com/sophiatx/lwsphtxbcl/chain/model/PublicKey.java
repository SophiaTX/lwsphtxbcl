/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sophiatx.lwsphtxbcl.encoding.KeyEncoder;
import com.sophiatx.lwsphtxbcl.encoding.exception.KeyFormatException;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigInteger;

@JsonIgnoreProperties({"key", "keyStr"})
public class PublicKey implements BinarySerializable
{
    // SPH1111111111111111111111111111111114T1Anm
    public static final PublicKey ZERO_KEY = new PublicKey(new BigInteger("0"));

    private final BigInteger key;
    private String keyStr;

    public PublicKey(BigInteger key)
    {
        this.key = key;
    }

    public PublicKey(String key)
    throws KeyFormatException
    {
        this.key = KeyEncoder.decodePublicKey(key);
        this.keyStr = key;
    }

    public BigInteger getKey()
    {
        return key;
    }

    @Override
    @JsonValue
    public String toString()
    {
        if (keyStr == null)
            this.keyStr = KeyEncoder.encodePublicKey(key);
        return keyStr;
    }

    @Override
    public byte[] toBytes()
    {
        return BinUtils.toBytesPadded(key, 33);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof PublicKey)) return false;
        return key.equals(((PublicKey) o).getKey());
    }

    @Override
    public int hashCode()
    {
        return key.hashCode();
    }
}
