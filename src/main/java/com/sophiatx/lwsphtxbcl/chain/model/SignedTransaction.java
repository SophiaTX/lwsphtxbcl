/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AbstractOperation;
import com.sophiatx.lwsphtxbcl.comm.Connector;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.BroadcastTransactionRequest;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.comm.exception.ConnectionException;
import com.sophiatx.lwsphtxbcl.comm.exception.ResponseErrorException;
import com.sophiatx.lwsphtxbcl.crypto.CompactSignature;
import com.sophiatx.lwsphtxbcl.crypto.KeyPair;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.SophiaTX.OBJECT_MAPPER;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

public class SignedTransaction extends Transaction
{
    @JsonCreator
    public SignedTransaction(
            @JsonProperty(KEY_REF_BLOCK_NUM) UShort refBlockNum,
            @JsonProperty(KEY_REF_BLOCK_PREFIX) UInteger refBlockPrefix,
            @JsonProperty(KEY_EXPIRATION) LocalDateTime expiration,
            @JsonProperty(KEY_OPERATIONS) List<AbstractOperation> operations,
            @JsonProperty(KEY_SIGNATURES) List<CompactSignature> signatures,
            @JsonProperty(KEY_EXTENSIONS) List<FutureExtension> extensions
    )
    {
        super(refBlockNum, refBlockPrefix, expiration, operations, extensions);
        verifyNotNull(signatures);
        if (operations.size() < 1)
            throw new IllegalStateException("Requires at least one operation");
        if (signatures.size() < 1)
            throw new IllegalStateException("Requires at least one signature");
        this.signatures.addAll(signatures);
    }

    public SignedTransaction(Transaction tx, CompactSignature signature)
    {
        super(tx.refBlockNum, tx.refBlockPrefix, tx.expiration, tx.operations, tx.getExtensions());
        verifyNotNull(signature);
        if (operations.size() < 1)
            throw new IllegalStateException("Requires at least one operation");
        this.signatures.add(signature);
    }

    /**
     * Sign a transaction
     *
     * @param kp      KeyPair of the signing account
     * @param chainId ID of a chain the transaction is valid for
     * @return Signed transaction
     */
    @Override
    public SignedTransaction sign(KeyPair kp, String chainId)
    {
        Objects.requireNonNull(kp, "Chain ID can not be null");
        Objects.requireNonNull(chainId, "Key pair can not be null");
        this.signatures.add(signTransaction(this, kp, chainId));
        return this;
    }

    public List<CompactSignature> getSignatures()
    {
        return signatures;
    }

    /**
     * Broadcast the transaction
     *
     * @param c Connector
     * @throws ConnectionException    An issue occurred while trying to connect to
     *                                the blockchain node
     * @throws ResponseErrorException Invalid TX, check error object
     * @throws CommunicationException Parent exception of all above plus rest of
     *                                the errors that may've occurred while
     *                                talking to the blockchain node
     */
    public SignedTransaction broadcast(Connector c)
    throws ConnectionException, ResponseErrorException, CommunicationException
    {
        for (int i = 0; i < operations.size(); i++) {
            AbstractOperation o = operations.get(i);
            if (o.getFee() == null) {
                throw new IllegalStateException("Fee for the operation " + i + " ( " +
                        o.getOperationType().toString() + " ) is not available. You " +
                        "need to calculate fees before broadcasting the transaction.");
            }
        }
        c.execute(new BroadcastTransactionRequest(this));
        return this;
    }

    // prevent accidental modifying of the object

    @Override
    public Transaction addOperation(AbstractOperation op)
    {
        unmodifiableException();
        return null;
    }

    @Override
    public Transaction expireAt(LocalDateTime exp)
    {
        unmodifiableException();
        return null;
    }

    @Override
    public Transaction expireIn(long s)
    {
        unmodifiableException();
        return null;
    }

    private static void unmodifiableException()
    throws IllegalArgumentException
    {
        throw new IllegalStateException("Signed transaction can no longer be modified " +
                "as this would render the already existing signatures invalid");
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this) return true;
        if (!(o instanceof SignedTransaction)) return false;
        SignedTransaction that = (SignedTransaction) o;
        return Objects.equals(refBlockNum, that.refBlockNum) &&
                Objects.equals(refBlockPrefix, that.refBlockPrefix) &&
                Objects.equals(expiration, that.expiration) &&
                Objects.equals(operations, that.operations) &&
                Objects.equals(extensions, that.extensions) &&
                Objects.equals(signatures, that.signatures);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(refBlockNum, refBlockPrefix, expiration, operations,
                extensions, signatures);
    }

    @Override
    public String toString()
    {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Unable to serialize the transaction", e);
        }
    }
}
