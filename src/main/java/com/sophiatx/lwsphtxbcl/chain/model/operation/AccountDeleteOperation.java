/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "account"})
public class AccountDeleteOperation extends AbstractFreeOperation
{
    @JsonProperty("account")
    private String account;

    @JsonCreator
    public AccountDeleteOperation(@JsonProperty("account") String account)
    {
        verifyNotNull(account);
        this.account = account;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.ACCOUNT_DELETE;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(account);
    }

    public String getAccount()
    {
        return account;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof AccountDeleteOperation)) return false;
        AccountDeleteOperation that = (AccountDeleteOperation) o;
        return Objects.equals(account, that.account);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(account);
    }
}
