/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "account", "witness", "approve"})
public class AccountWitnessVoteOperation extends AbstractFreeOperation
{
    @JsonProperty("account")
    private String account;
    @JsonProperty("witness")
    private String witness;
    @JsonProperty("approve")
    private boolean approve;

    @JsonCreator
    public AccountWitnessVoteOperation(
            @JsonProperty("account") String account,
            @JsonProperty("witness") String witness,
            @JsonProperty("approve") boolean approve
    )
    {
        verifyNotNull(account, witness);
        this.account = account;
        this.witness = witness;
        this.approve = approve;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.ACCOUNT_WITNESS_VOTE;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(account).append(witness).append(approve);
    }

    public String getAccount()
    {
        return account;
    }

    public String getWitness()
    {
        return witness;
    }

    public boolean isApprove()
    {
        return approve;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof AccountWitnessVoteOperation)) return false;
        AccountWitnessVoteOperation that = (AccountWitnessVoteOperation) o;
        return Objects.equals(that.account, account) &&
                Objects.equals(that.witness, witness) &&
                Objects.equals(that.approve, approve);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(account, witness, approve);
    }
}
