/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "accountToRecover", "newRecoveryAccount", "extensions"})
public class ChangeRecoveryAccountOperation extends AbstractFreeOperation
{
    @JsonProperty("account_to_recover")
    private String accountToRecover;
    @JsonProperty("new_recovery_account")
    private String newRecoveryAccount;
    @JsonProperty("extensions")
    private List<FutureExtension> extensions;

    public ChangeRecoveryAccountOperation(
            @JsonProperty("account_to_recover") String accountToRecover,
            @JsonProperty("new_recovery_account") String newRecoveryAccount,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        this(accountToRecover, newRecoveryAccount);
        this.extensions = extensions;
    }

    public ChangeRecoveryAccountOperation(String accountToRecover, String newRecoveryAccount)
    {
        verifyNotNull(accountToRecover, newRecoveryAccount);
        this.accountToRecover = accountToRecover;
        this.newRecoveryAccount = newRecoveryAccount;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.CHANGE_RECOVERY_ACCOUNT;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(accountToRecover).append(newRecoveryAccount).append(extensions);
    }

    public String getAccountToRecover()
    {
        return accountToRecover;
    }

    public String getNewRecoveryAccount()
    {
        return newRecoveryAccount;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ChangeRecoveryAccountOperation)) return false;
        ChangeRecoveryAccountOperation that = (ChangeRecoveryAccountOperation) o;
        return Objects.equals(accountToRecover, that.accountToRecover) &&
                Objects.equals(newRecoveryAccount, that.newRecoveryAccount) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(accountToRecover, newRecoveryAccount, extensions);
    }
}
