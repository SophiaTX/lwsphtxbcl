/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "recovery_account", "account_to_recover", "new_owner_authority", "extensions"})
public class RequestAccountRecovery extends AbstractFreeOperation
{
    @JsonProperty("recovery_account")
    private String recoveryAccount;
    @JsonProperty("account_to_recover")
    private String accountToRecover;
    @JsonProperty("new_owner_authority")
    private Authority newOwnerAuthority;
    @JsonProperty("extensions")
    private List<FutureExtension> extensions = new ArrayList<>();

    @JsonCreator
    public RequestAccountRecovery(
            @JsonProperty("recovery_account") String recoveryAccount,
            @JsonProperty("account_to_recover") String accountToRecover,
            @JsonProperty("new_owner_authority") Authority newOwnerAuthority,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        this(recoveryAccount, accountToRecover, newOwnerAuthority);
        this.extensions = extensions;
    }

    public RequestAccountRecovery(String recoveryAccount, String accountToRecover, Authority newOwnerAuthority)
    {
        verifyNotNull(recoveryAccount, accountToRecover, newOwnerAuthority);
        this.recoveryAccount = recoveryAccount;
        this.accountToRecover = accountToRecover;
        this.newOwnerAuthority = newOwnerAuthority;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.REQUEST_ACCOUNT_RECOVERY;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(recoveryAccount).append(accountToRecover).append(newOwnerAuthority)
                .append(extensions);
    }

    public String getRecoveryAccount()
    {
        return recoveryAccount;
    }

    public String getAccountToRecover()
    {
        return accountToRecover;
    }

    public Authority getNewOwnerAuthority()
    {
        return newOwnerAuthority;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof RequestAccountRecovery)) return false;
        RequestAccountRecovery that = (RequestAccountRecovery) o;
        return Objects.equals(recoveryAccount, that.recoveryAccount) &&
                Objects.equals(accountToRecover, that.accountToRecover) &&
                Objects.equals(newOwnerAuthority, that.newOwnerAuthority) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(recoveryAccount, accountToRecover, newOwnerAuthority, extensions);
    }
}
