/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "resetAccount", "accountToReset", "newOwnerAuthority"})
public class ResetAccountOperation extends AbstractFreeOperation
{
    @JsonProperty("reset_account")
    private String resetAccount;
    @JsonProperty("account_to_reset")
    private String accountToReset;
    @JsonProperty("new_owner_authority")
    private Authority newOwnerAuthority;

    @JsonCreator
    public ResetAccountOperation(
            @JsonProperty("reset_account") String resetAccount,
            @JsonProperty("account_to_reset") String accountToReset,
            @JsonProperty("new_owner_authority") Authority newOwnerAuthority
    )
    {
        verifyNotNull(resetAccount, accountToReset, newOwnerAuthority);
        this.resetAccount = resetAccount;
        this.accountToReset = accountToReset;
        this.newOwnerAuthority = newOwnerAuthority;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.RESET_ACCOUNT;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(resetAccount).append(accountToReset).append(newOwnerAuthority);
    }

    public String getResetAccount()
    {
        return resetAccount;
    }

    public String getAccountToReset()
    {
        return accountToReset;
    }

    public Authority getNewOwnerAuthority()
    {
        return newOwnerAuthority;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ResetAccountOperation)) return false;
        ResetAccountOperation that = (ResetAccountOperation) o;
        return Objects.equals(resetAccount, that.resetAccount) &&
                Objects.equals(accountToReset, that.accountToReset) &&
                Objects.equals(newOwnerAuthority, that.newOwnerAuthority);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(resetAccount, accountToReset, newOwnerAuthority);
    }
}
