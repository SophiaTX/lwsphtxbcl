/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "owner"})
public class WitnessStopOperation extends AbstractOperation
{
    @JsonProperty("owner")
    private String owner;

    @JsonCreator
    public WitnessStopOperation(@JsonProperty("owner") String owner)
    {
        verifyNotNull(owner);
        this.owner = owner;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.WITNESS_STOP;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(owner);
    }

    public String getOwner()
    {
        return owner;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WitnessStopOperation that = (WitnessStopOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, owner);
    }
}
