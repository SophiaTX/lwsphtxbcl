/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation.virtual;

import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AbstractFreeOperation;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

/**
 * Base class for virtual ( chain-generated ) operations.
 *
 * This class extends {@link AbstractFreeOperation}, implements and overrides
 * unnecessary methods related to binary serialization.
 *
 * Binary would normally be used to crete transaction IDs and signatures, none
 * of which applies to the virtual operations from a user perspective.
 */
public abstract class AbstractVirtualOperation extends AbstractFreeOperation
{
    @Override
    public byte[] toBytes()
    {
        return new byte[0];
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        // nothing to build here
    }

    @Override
    public void calculateFee(ConversionRate rate)
    {
        // nothing to calculate
    }
}
