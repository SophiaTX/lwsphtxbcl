/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation.virtual;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "fromAccount", "toAccount", "withdrawn", "deposited"})
public class FillVestingWithdrawOperation extends AbstractVirtualOperation
{
    @JsonProperty("from_account")
    private String fromAccount;
    @JsonProperty("to_account")
    private String toAccount;
    @JsonProperty("withdrawn")
    private Asset withdrawn;
    @JsonProperty("deposited")
    private Asset deposited;

    @JsonCreator
    public FillVestingWithdrawOperation(
            @JsonProperty("from_account") String fromAccount,
            @JsonProperty("to_account") String toAccount,
            @JsonProperty("withdrawn") Asset withdrawn,
            @JsonProperty("deposited") Asset deposited
    )
    {
        verifyNotNull(fromAccount, toAccount, withdrawn, deposited);
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.withdrawn = withdrawn;
        this.deposited = deposited;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.FILL_VESTING_WITHDRAW;
    }

    public String getFromAccount()
    {
        return fromAccount;
    }

    public String getToAccount()
    {
        return toAccount;
    }

    public Asset getWithdrawn()
    {
        return withdrawn;
    }

    public Asset getDeposited()
    {
        return deposited;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof FillVestingWithdrawOperation)) return false;
        FillVestingWithdrawOperation that = (FillVestingWithdrawOperation) o;
        return Objects.equals(fromAccount, that.fromAccount) &&
                Objects.equals(toAccount, that.toAccount) &&
                Objects.equals(withdrawn, that.withdrawn) &&
                Objects.equals(deposited, that.deposited);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fromAccount, toAccount, withdrawn, deposited);
    }
}
