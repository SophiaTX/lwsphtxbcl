/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation.virtual;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "producer", "vestingShares"})
public class ProducerRewardOperation extends AbstractVirtualOperation
{
    @JsonProperty("producer")
    private String producer;
    @JsonProperty("vesting_shares")
    private Asset vestingShares;

    @JsonCreator
    public ProducerRewardOperation(
            @JsonProperty("producer") String producer,
            @JsonProperty("vesting_shares") Asset vestingShares
    )
    {
        verifyNotNull(producer, vestingShares);
        this.producer = producer;
        this.vestingShares = vestingShares;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.PRODUCER_REWARD;
    }

    public String getProducer()
    {
        return producer;
    }

    public Asset getVestingShares()
    {
        return vestingShares;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ProducerRewardOperation)) return false;
        ProducerRewardOperation that = (ProducerRewardOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(producer, that.producer) &&
                Objects.equals(vestingShares, that.vestingShares);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, producer, vestingShares);
    }
}
