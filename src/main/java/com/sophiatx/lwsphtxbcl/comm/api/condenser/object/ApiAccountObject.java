/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.chain.model.PublicKey;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.util.List;
import java.util.Objects;

/**
 * This object should only be instantiated by Jackson using reflection
 */
@JsonIgnoreProperties({"transfer_history", "other_history"})
public class ApiAccountObject
{
    @JsonProperty("id")
    private UInteger id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("owner")
    private Authority owner;

    @JsonProperty("active")
    private Authority active;

    @JsonProperty("memo_key")
    private PublicKey memoKey;

    @JsonProperty("json_metadata")
    private JsonNode jsonMetadata;

    @JsonProperty("voting_proxy")
    private String votingProxy;

    @JsonProperty("balance")
    private Asset balance;

    @JsonProperty("sponsored_accounts")
    private List<String> sponsoredAccounts;

    @JsonProperty("sponsoring_account")
    private String sponsoringAccount;

    @JsonProperty("witness_votes")
    private List<String> witnessVotes;

    @JsonProperty("vesting_shares")
    private Asset vestingShares;

    @JsonProperty("vesting_withdraw_rate")
    private Asset vestingWithdrawRate;

    @JsonProperty("to_withdraw")
    private Long toWithdraw;

    @JsonProperty("vesting_balance")
    private Asset vestingBalance;

    public UInteger getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Authority getOwner()
    {
        return owner;
    }

    public Authority getActive()
    {
        return active;
    }

    public PublicKey getMemoKey()
    {
        return memoKey;
    }

    public JsonNode getJsonMetadata()
    {
        return jsonMetadata;
    }

    public String getVotingProxy()
    {
        return votingProxy;
    }

    public Asset getBalance()
    {
        return balance;
    }

    public List<String> getSponsoredAccounts()
    {
        return sponsoredAccounts;
    }

    public String getSponsoringAccount()
    {
        return sponsoringAccount;
    }

    public List<String> getWitnessVotes()
    {
        return witnessVotes;
    }

    public Asset getVestingShares()
    {
        return vestingShares;
    }

    public Asset getVestingWithdrawRate()
    {
        return vestingWithdrawRate;
    }

    public Long getToWithdraw()
    {
        return toWithdraw;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ApiAccountObject)) return false;
        ApiAccountObject that = (ApiAccountObject) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(owner, that.owner) &&
                Objects.equals(active, that.active) &&
                Objects.equals(memoKey, that.memoKey) &&
                Objects.equals(jsonMetadata, that.jsonMetadata) &&
                Objects.equals(votingProxy, that.votingProxy) &&
                Objects.equals(balance, that.balance) &&
                Objects.equals(sponsoredAccounts, that.sponsoredAccounts) &&
                Objects.equals(sponsoringAccount, that.sponsoringAccount) &&
                Objects.equals(witnessVotes, that.witnessVotes) &&
                Objects.equals(vestingShares, that.vestingShares) &&
                Objects.equals(vestingWithdrawRate, that.vestingWithdrawRate) &&
                Objects.equals(toWithdraw, that.toWithdraw);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, owner, active, memoKey, jsonMetadata, votingProxy, balance, sponsoredAccounts, sponsoringAccount, witnessVotes, vestingShares, vestingWithdrawRate, toWithdraw);
    }
}
