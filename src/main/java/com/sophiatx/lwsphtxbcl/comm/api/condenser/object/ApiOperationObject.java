/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sophiatx.lwsphtxbcl.chain.Configuration;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AbstractOperation;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * This object should only be instantiated by Jackson using reflection
 */
@JsonIgnoreProperties("legacy_operation")
public class ApiOperationObject
{
    @JsonProperty("trx_id")
    private String transactionId;

    @JsonProperty("block")
    private UInteger block;

    @JsonProperty("trx_in_block")
    private UInteger transactionInBlock;

    @JsonProperty("op_in_trx")
    private UShort operationInTransaction;

    @JsonProperty("virtual_op")
    private BigInteger virtualOperation;

    @JsonProperty("timestamp")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = Configuration.DATE_FORMAT, timezone = Configuration.TIME_ZONE)
    private LocalDateTime timestamp;

    @JsonProperty("fee_payer")
    private String feePayer;

    @JsonProperty("op")
    private AbstractOperation operation;

    public String getTransactionId()
    {
        return transactionId;
    }

    public UInteger getBlock()
    {
        return block;
    }

    public UInteger getTransactionInBlock()
    {
        return transactionInBlock;
    }

    public UShort getOperationInTransaction()
    {
        return operationInTransaction;
    }

    public BigInteger getVirtualOperation()
    {
        return virtualOperation;
    }

    public LocalDateTime getTimestamp()
    {
        return timestamp;
    }

    public String getFeePayer()
    {
        return feePayer;
    }

    public AbstractOperation getOperation()
    {
        return operation;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ApiOperationObject)) return false;
        ApiOperationObject that = (ApiOperationObject) o;
        return Objects.equals(transactionId, that.transactionId) &&
                Objects.equals(block, that.block) &&
                Objects.equals(transactionInBlock, that.transactionInBlock) &&
                Objects.equals(operationInTransaction, that.operationInTransaction) &&
                Objects.equals(virtualOperation, that.virtualOperation) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(feePayer, that.feePayer) &&
                Objects.equals(operation, that.operation);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(transactionId, block, transactionInBlock, operationInTransaction,
                virtualOperation, timestamp, feePayer, operation);
    }
}
