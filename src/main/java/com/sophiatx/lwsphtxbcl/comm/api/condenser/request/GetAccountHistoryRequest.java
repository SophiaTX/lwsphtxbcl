/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.request;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sophiatx.lwsphtxbcl.comm.AbstractMappableRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiOperationObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.OrderedObject;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

public class GetAccountHistoryRequest
        extends AbstractMappableRequest<Collection<OrderedObject<ApiOperationObject>>>
{
    private String accountName;
    private long from;
    private long limit;

    public GetAccountHistoryRequest(String accountName, long from, long limit)
    {
        verifyNotNull(accountName);
        verifyPrecondition(from <= UInteger.MAX_VALUE, "From param out of range");
        verifyPrecondition(limit > 0 && limit <= UInteger.MAX_VALUE, "Limit param out of range");
        this.accountName = accountName;
        this.from = from;
        this.limit = limit - 1; // workaround for a bug in the condenser API
    }

    public GetAccountHistoryRequest older()
    {
        long newFrom = Math.max(from - limit, 1);
        long newLimit = Math.min(newFrom, limit);
        return new GetAccountHistoryRequest(accountName, newFrom, newLimit);
    }

    public GetAccountHistoryRequest newer()
    {
        long newFrom = Math.min(from + limit, UInteger.MAX_VALUE);
        return new GetAccountHistoryRequest(accountName, newFrom, limit);
    }

    @Override
    public String getMethodName()
    {
        return "get_account_history";
    }

    @Override
    public Object getParams()
    {
        return Arrays.asList(accountName, from, limit);
    }

    @Override
    public JavaType getTargetType(TypeFactory tf)
    {
        return tf.constructCollectionType(
                List.class,
                tf.constructParametricType(OrderedObject.class, ApiOperationObject.class)
        );
    }
}
