/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.database.request;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.comm.AbstractMappableRequest;

import static com.sophiatx.lwsphtxbcl.SophiaTX.OBJECT_MAPPER;

public class GetCurrentPriceFeedRequest extends AbstractMappableRequest<ConversionRate>
{
    private ObjectNode params = OBJECT_MAPPER.createObjectNode();

    public GetCurrentPriceFeedRequest(AssetSymbol currency)
    {
        params.put("symbol", currency.toString());
    }

    @Override
    public JavaType getTargetType(TypeFactory tf)
    {
        return tf.constructType(ConversionRate.class);
    }

    @Override
    public String getMethodName()
    {
        return "get_current_price_feed";
    }

    @Override
    public Object getParams()
    {
        return params;
    }
}
