/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.exception;

import java.io.IOException;

/**
 * Top level exception used for reporting exceptions that occur while trying to
 * talk to the blockchain
 */
public class CommunicationException extends IOException
{
    public CommunicationException(String s)
    {
        super(s);
    }

    public CommunicationException(Throwable throwable)
    {
        super(throwable);
    }

    public CommunicationException(String s, Throwable throwable)
    {
        super(s, throwable);
    }
}
