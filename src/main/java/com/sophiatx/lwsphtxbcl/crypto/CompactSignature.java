/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.NumUtils;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonIgnoreProperties({"v", "r", "s"})
public class CompactSignature implements BinarySerializable
{
    private static final int COMPACT_CANONICAL_VAL = 27 + 4;

    private final int v;
    private final BigInteger r;
    private final BigInteger s;

    public CompactSignature(int recId, BigInteger r, BigInteger s)
    {
        validateComponents(recId, r, s);
        this.v = recId2v(recId);
        this.r = r;
        this.s = toCanonicalised(s);
    }

    @JsonCreator
    public CompactSignature(String hex)
    {
        byte[] binary = Hex.toBytes(hex);
        int v = (int) binary[0];
        BigInteger r = NumUtils.toBigInt(Arrays.copyOfRange(binary, 1, 33));
        BigInteger s = NumUtils.toBigInt(Arrays.copyOfRange(binary, 33, 65));

        validateComponents(v2recId(v), r, s);

        this.v = v;
        this.r = r;
        this.s = toCanonicalised(s);
    }

    /**
     * Will automatically adjust the S component to be less than or equal to
     * half the curve order, if necessary.
     *
     * This is required because for every signature (r,s)
     * the signature (r, -s (mod N)) is a valid signature of the same message.
     *
     * @return the S component in a canonicalized form.
     */
    private static BigInteger toCanonicalised(BigInteger s)
    {
        if (!isCanonical(s)) {
            // The order of the curve is the number of valid points that exist
            // on that curve. If S is in the upper half of the number of valid
            // points, then bring it back to the lower half. Imagine that:
            //    N = 10
            //    s = 8, so (-8 % 10 == 2) thus both (r, 8) and (r, 2) are valid solutions.
            //    10 - 8 == 2, giving us always the latter solution, which is canonical.
            return Keys.SPEC.getN().subtract(s);
        } else {
            return s;
        }
    }

    /**
     * Verify that the S component of the signature is canonical
     *
     * @return Whether the S component is canonical
     */
    private static boolean isCanonical(BigInteger s)
    {
        return s.compareTo(Keys.HALF_CURVE_ORDER) <= 0;
    }

    /**
     * Kind of "canonical" verification implemented by SophiaTX ( STEEM ).
     * Doesn't really tell whether the signature is canonical or not.
     * Blame the blockchain team: https://github.com/steemit/steem/issues/1944
     *
     * @return Whether the blockchain will recognize the signature as canonical
     */
    public boolean isAcceptableByBlockchain()
    {
        byte[] sig = toBytes();
        return (
                ((sig[1] & 0x80) == 0)
                        && !(sig[1] == 0 && ((sig[2] & 0x80) == 0))
                        && ((sig[33] & 0x80) == 0)
                        && !(sig[33] == 0 && ((sig[34] & 0x80) == 0))
        );
    }

    private void validateComponents(int recId, BigInteger r, BigInteger s)
    {
        verifyPrecondition(recId >= 0, "recId must be >= 0");
        verifyPrecondition(recId <= 4, "recId must be <= 4");
        verifyPrecondition(r.signum() >= 0, "r must be positive");
        verifyPrecondition(s.signum() >= 0, "s must be positive");
    }

    private int v2recId(int v)
    {
        return v - COMPACT_CANONICAL_VAL;
    }

    private int recId2v(int recId)
    {
        return recId + COMPACT_CANONICAL_VAL;
    }

    public int getV()
    {
        return v;
    }

    public int getRecId()
    {
        return v - 27 - 4;
    }

    public BigInteger getR()
    {
        return r;
    }

    public BigInteger getS()
    {
        return s;
    }

    public boolean verify(Sha256Hash digest, BigInteger pubKey)
    {
        BigInteger recoveredPublicKey = Sign.recoverPublicKey(this, digest.getBytes());
        if (recoveredPublicKey == null)
            return false;
        return recoveredPublicKey.equals(pubKey);
    }

    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append((byte) v);
        bytes.append(BinUtils.toBytesPadded(r, 32));
        bytes.append(BinUtils.toBytesPadded(s, 32));
        return bytes.getBytes();
    }

    @Override
    @JsonValue
    public String toString()
    {
        return Hex.toString(toBytes());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof CompactSignature)) return false;
        CompactSignature that = (CompactSignature) o;
        return v == that.v &&
                Objects.equals(r, that.r) &&
                Objects.equals(s, that.s);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(v, r, s);
    }
}
