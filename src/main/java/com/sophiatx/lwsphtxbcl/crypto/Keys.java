/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import com.sophiatx.lwsphtxbcl.utils.NumUtils;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;

public class Keys
{
    static final ECParameterSpec SPEC = ECNamedCurveTable.getParameterSpec("secp256k1");
    static final ECDomainParameters PARAMS = new ECDomainParameters(
            SPEC.getCurve(),
            SPEC.getG(),
            SPEC.getN(),
            SPEC.getH()
    );
    static final ECCurve CURVE = SPEC.getCurve();
    static final BigInteger HALF_CURVE_ORDER = SPEC.getN().shiftRight(1);

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * Create a keypair using SECP-256k1 curve.
     */
    static KeyPair createSecp256k1KeyPair()
    throws NoSuchProviderException,
           NoSuchAlgorithmException,
           InvalidAlgorithmParameterException
    {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", "BC");
        ECGenParameterSpec ecGenParameterSpec = new ECGenParameterSpec("secp256k1");
        keyPairGenerator.initialize(ecGenParameterSpec, SecureRandomUtils.secureRandom());
        return keyPairGenerator.generateKeyPair();
    }

    /**
     * Computes a public key from the given private key
     *
     * @param privKey private key
     * @return BigInteger compressed public key
     */
    public static BigInteger publicKeyFromPrivate(BigInteger privKey)
    {
        ECPoint point = SPEC.getG().multiply(privKey);
        return NumUtils.toBigInt(point.getEncoded(true));
    }
}
