/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.encoding;

import java.math.BigInteger;
import java.util.regex.Pattern;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

public class Hex
{
    private static final Pattern PATTERN = Pattern.compile("^[0-9a-fA-F]+$");

    public static byte[] toBytes(String input)
    {
        int len = input.length();

        if (len == 0) {
            return new byte[]{};
        }

        byte[] data;
        int startIdx;
        if (len % 2 != 0) {
            data = new byte[(len / 2) + 1];
            data[0] = (byte) Character.digit(input.charAt(0), 16);
            startIdx = 1;
        } else {
            data = new byte[len / 2];
            startIdx = 0;
        }

        for (int i = startIdx; i < len; i += 2) {
            data[(i + 1) / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4)
                    + Character.digit(input.charAt(i + 1), 16));
        }
        return data;
    }

    public static String toString(byte[] input, int offset, int length)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = offset; i < offset + length; i++) {
            stringBuilder.append(String.format("%02x", input[i] & 0xFF));
        }

        return stringBuilder.toString();
    }

    public static String toString(byte[] input)
    {
        return toString(input, 0, input.length);
    }

    public static String toString(BigInteger value)
    {
        return value.toString(16);
    }

    /**
     * Validate whether the input is a valid HEX string of given length.
     * Set the length to 0 if you don't want to validate the length.
     *
     * @param str          String to validate
     * @param binaryLength Expected len ( in bytes, 0 = ignore )
     * @return Validation result
     */
    public static boolean isValid(String str, int binaryLength)
    {
        verifyPrecondition(binaryLength > -1,
                "Length must be a positive number, 0 = any length");
        if (binaryLength > 0 && (binaryLength * 2) != str.length())
            return false;
        return PATTERN.matcher(str).find();
    }

    /**
     * Validate whether the input is a valid HEX string.
     *
     * @param str String to validate
     * @return Validation result
     */
    public static boolean isValid(String str)
    {
        return isValid(str, 0);
    }
}
