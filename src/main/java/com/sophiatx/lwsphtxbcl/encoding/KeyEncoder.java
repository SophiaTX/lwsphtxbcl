/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.encoding;

import com.sophiatx.lwsphtxbcl.crypto.Ripemd160Hash;
import com.sophiatx.lwsphtxbcl.crypto.Sha256Hash;
import com.sophiatx.lwsphtxbcl.encoding.exception.KeyFormatException;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.NumUtils;

import java.math.BigInteger;
import java.util.Arrays;

public class KeyEncoder
{
    private static final int WIF_STR_LENGTH = 51;
    private static final int WIF_HEADER = 0x80;
    private static final String PUBLIC_KEY_PREFIX = "SPH";
    private static final int PUBLIC_KEY_STR_LENGTH = 50 + PUBLIC_KEY_PREFIX.length();

    /**
     * WIF key consists of: base58(0x80 + byte[32] key + byte[4] checksum)
     * The checksum is calculated as first4bytes(sha256(sha256(0x80+key)))
     *
     * @param key Private key
     * @return WIF-encoded private key
     */
    public static String encodeWif(BigInteger key)
    {
        // first of all, convert the numeric representation to 32B binary
        byte[] privateKey = BinUtils.toBytesPadded(key, 32);

        // 1B for WIF_HEADER, 32B for actual private key, 4B for checksum
        byte[] assembled = new byte[37];
        // set the first byte to standardized WIF header
        assembled[0] = (byte) WIF_HEADER;
        // populate the next 32 bytes of the buffer with the actual private key
        System.arraycopy(privateKey, 0, assembled, 1, 32);
        // take the header byte + private key and double-sha256-hash them
        byte[] checksum = Sha256Hash.hashTwice(
                Arrays.copyOfRange(assembled, 0, 33) // drop the last 4 bytes
        );
        // populate the last 4 bytes of the buffer with the first 4 bytes of
        // the hash we calculated in the previous step - this is the checksum
        System.arraycopy(checksum, 0, assembled, 33, 4);

        // serialize the buffer using base58 encoding
        return Base58.encode(assembled);
    }

    /**
     * @param wif Private key encoded using Wallet Import Format
     * @return Decoded private key
     * @throws KeyFormatException If the key format/checksum is invalid
     */
    public static BigInteger decodeWif(String wif)
    throws KeyFormatException
    {
        if (wif.length() != WIF_STR_LENGTH)
            throw new KeyFormatException("WIF key must be " + WIF_STR_LENGTH + " characters long");

        byte[] decoded = Base58.decode(wif); // 37 bytes
        verifyWifHeader(decoded);
        verifyWifChecksum(decoded);

        // take the bytes 1-33 ( actual private key which is 32 bytes ) ignoring
        // byte 0 ( WIF header ) and the last 4 bytes ( checksum )
        byte[] privateKey = Arrays.copyOfRange(decoded, 1, 33);

        return NumUtils.toBigInt(privateKey);
    }

    /**
     * WIF keys must start with {@link #WIF_HEADER} header byte to be considered valid
     *
     * @param wif Binary wif key
     */
    private static void verifyWifHeader(byte[] wif)
    {
        if (wif[0] != (byte) WIF_HEADER)
            throw new KeyFormatException("WIF key must start with " + WIF_HEADER + " byte!");
    }

    /**
     * WIF keys use a checksum to prevent possible typos. The checksum is stored
     * in the last 4 bytes of the hex key and must match the data stored before
     * the checksum bytes for the WIF key to be considered valid
     *
     * @param wif Binary wif key
     */
    private static void verifyWifChecksum(byte[] wif)
    {
        byte[] data = Arrays.copyOfRange(wif, 0, 33);
        byte[] checksum = Arrays.copyOfRange(wif, 33, 37);
        byte[] actualChecksum = Arrays.copyOfRange(Sha256Hash.hashTwice(data), 0, 4);

        if (!Arrays.equals(checksum, actualChecksum))
            throw new KeyFormatException("Invalid checksum");
    }

    /**
     * Public key consists of: SPH+base58(byte[33] key + 4 bytes checksum)
     * The checksum is calculated as first4bytes(ripemd160(key))
     *
     * @param publicKey Public key
     * @return Serialized public key
     */
    public static String encodePublicKey(BigInteger publicKey)
    {
        // first of all, convert the numeric representation to 33B binary
        byte[] binaryKey = BinUtils.toBytesPadded(publicKey, 33);

        // 33 bytes for compressed public key + 4 bytes for checksum
        byte[] assembled = new byte[37];
        // populate the first 33 bytes with the compressed PK
        System.arraycopy(binaryKey, 0, assembled, 0, 33);
        // populate the last 4 bytes with the first 4 bytes of the PK hash -
        // this is our checksum which we can use to verify integrity of the key
        System.arraycopy(Ripemd160Hash.hash(binaryKey), 0, assembled, 33, 4);

        // serialize the buffer using base58 encoding and prepend the prefix
        return PUBLIC_KEY_PREFIX + Base58.encode(assembled);
    }

    /**
     * @param pk Serialized public key
     * @return Decoded public key
     * @throws KeyFormatException If the format/checksum is invalid
     */
    public static BigInteger decodePublicKey(final String pk)
    throws KeyFormatException
    {
        if (pk.length() != PUBLIC_KEY_STR_LENGTH)
            throw new KeyFormatException("Public key must be " + PUBLIC_KEY_STR_LENGTH + " characters long");

        if (!pk.startsWith(PUBLIC_KEY_PREFIX))
            throw new KeyFormatException("Public key must start with " + PUBLIC_KEY_PREFIX);

        byte[] decoded = Base58.decode(removePublicKeyPrefix(pk)); // 37 bytes
        verifyPublicKeyHeaderByte(decoded);
        verifyPublicKeyChecksum(decoded);

        // take the first 33 bytes ( compressed pub. key ) dropping the checksum
        byte[] publicKey = Arrays.copyOfRange(decoded, 0, 33);

        return NumUtils.toBigInt(publicKey);
    }

    /**
     * Serialized public keys start with a {@link KeyEncoder#PUBLIC_KEY_PREFIX}
     * which we need to remove before trying to run it through base58 decoder
     *
     * @param k Serialized public key
     * @return Serialized public key without the prefix
     */
    private static String removePublicKeyPrefix(final String k)
    {
        return k.substring(PUBLIC_KEY_PREFIX.length(), PUBLIC_KEY_STR_LENGTH);
    }

    /**
     * Compressed public key header must be either 0x02 or 0x03 to be considered
     * valid. This Header is used to decompress the public key when needed.
     *
     * @param pk Binary public key ( with or without checksum )
     */
    private static void verifyPublicKeyHeaderByte(byte[] pk)
    {
        if (pk[0] != 0x03 && pk[0] != 0x02)
            throw new KeyFormatException("Invalid public key header byte!");
    }

    /**
     * Serialized public keys use a checksum to prevent possible typos. The
     * checksum is stored in the last 4 bytes of the serialized public key and
     * must match the data stored before the checksum for the public key key
     * to be considered valid. Unlike WIF keys, public keys don't use double
     * hashing, they use single sha256 pass instead.
     *
     * @param pk Binary public key ( with checksum )
     */
    private static void verifyPublicKeyChecksum(byte[] pk)
    {
        // take all the bytes except the last 4 ones ( checksum )
        byte[] data = Arrays.copyOfRange(pk, 0, 33);
        // take only the last 4 bytes ( checksum )
        byte[] checksum = Arrays.copyOfRange(pk, 33, 37);
        // take the first 4 bytes of a sha256 hash - this is the actual checksum
        byte[] actualChecksum = Arrays.copyOfRange(Ripemd160Hash.hash(data), 0, 4);

        // the calculated ( actual ) checksum must match the one stored in the
        // last 4 bytes of the serialized public key
        if (!Arrays.equals(checksum, actualChecksum))
            throw new KeyFormatException("Invalid checksum");
    }
}
