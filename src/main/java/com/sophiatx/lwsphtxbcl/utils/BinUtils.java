/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class BinUtils
{
    /**
     * Converts given BigInteger to binary of the given length
     *
     * @param value  Numerical value
     * @param length Binary length
     * @return Binary
     */
    public static byte[] toBytesPadded(BigInteger value, int length)
    {
        byte[] result = new byte[length];
        byte[] bytes = value.toByteArray();

        int bytesLength;
        int srcOffset;
        if (bytes[0] == 0) {
            bytesLength = bytes.length - 1;
            srcOffset = 1;
        } else {
            bytesLength = bytes.length;
            srcOffset = 0;
        }

        if (bytesLength > length) {
            throw new RuntimeException("Input is too large to put in byte array of size " + length);
        }

        int destOffset = length - bytesLength;
        System.arraycopy(bytes, srcOffset, result, destOffset, bytesLength);
        return result;
    }

    /**
     * Copies the given byte arrays into a one big byte array
     *
     * @param arrays Byte arrays to concat
     * @return Binary of all given byte arrays
     */
    public static byte[] concat(byte[]... arrays)
    {
        int length = 0;
        for (byte[] array : arrays) {
            length += array.length;
        }
        byte[] result = new byte[length];
        int pos = 0;
        for (byte[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }

    /**
     * Appends the given byte to the given byte array
     *
     * @param bytes Byte array to append to
     * @param b     Byte to append
     * @return Binary with appended byte
     */
    public static byte[] add(byte[] bytes, byte b)
    {
        byte[] result = new byte[bytes.length + 1];
        System.arraycopy(bytes, 0, result, 0, bytes.length);
        result[bytes.length] = b;
        return result;
    }

    /**
     * Converts given long to binary
     *
     * @param value int64
     * @return Binary
     */
    public static byte[] toBytes(long value)
    {
        return ByteBuffer.allocate(8)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putLong(value)
                .array();
    }

    /**
     * Converts given boolean to byte
     *
     * @param value boolean
     * @return Binary
     */
    public static byte toByte(boolean value)
    {
        return (byte) (value ? 0x01 : 0x00);
    }

    /**
     * Converts given integer to binary
     *
     * @param value int32
     * @return Binary
     */
    public static byte[] toBytes(int value)
    {
        return ByteBuffer.allocate(4)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putInt(value)
                .array();
    }

    /**
     * Converts given short to binary
     *
     * @param value int16
     * @return Binary
     */
    public static byte[] toBytes(short value)
    {
        return ByteBuffer.allocate(2)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putShort(value)
                .array();
    }

    /**
     * Converts given string to UTF-8 binary
     *
     * @param withLength Whether to prepend string length ( encoded as varint )
     *                   to the resulting binary
     * @param value      String to encode
     * @return Binary of UTF-8 encoded string
     */
    public static byte[] toBytes(String value, boolean withLength)
    {
        if (value == null) return new byte[1]; // 0x00 = zero length string
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        // string length encoded as varint
        if (withLength)
            bytes.append(toVarIntBytes(value.length()));
        // the rest is the actual string
        bytes.append(value.getBytes(StandardCharsets.UTF_8));
        return bytes.getBytes();
    }

    /**
     * Converts given string to UTF-8 binary
     *
     * @param value String to encode
     * @return Binary of UTF-8 encoded string
     */
    public static byte[] toBytes(String value)
    {
        return toBytes(value, false);
    }

    /**
     * Converts given long to var-int encoded binary
     *
     * https://developers.google.com/protocol-buffers/docs/encoding#varints
     *
     * @param value value int64
     * @return The byte representation of the given value.
     */
    public static byte[] toVarIntBytes(long value)
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();

        while ((value & 0xFFFFFFFFFFFFFF80L) != 0L) {
            bytes.append((byte) (((int) value & 0x7F) | 0x80));
            value >>>= 7;
        }

        bytes.append((byte) ((int) value & 0x7F));

        return bytes.getBytes();
    }

    /**
     * Converts given int to var-int encoded binary
     *
     * https://developers.google.com/protocol-buffers/docs/encoding#varints
     *
     * @param value value int32
     * @return The byte representation of the given value.
     */
    public static byte[] toVarIntBytes(int value)
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        int val = value;

        while ((val & 0xFFFFFF80) != 0L) {
            bytes.append((byte) ((val & 0x7F) | 0x80));
            val >>>= 7;
        }

        bytes.append((byte) (value & 0x7F));

        return bytes.getBytes();
    }

    /**
     * Change the order of a byte to little endian
     *
     * @param byteValue The byte to transform
     * @return The byte in its little endian representation
     */
    public static byte toLittleEndian(byte byteValue)
    {
        return ByteBuffer.allocate(1)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(byteValue)
                .get(0);
    }

    /**
     * Null-safe converter from {@link BinarySerializable} object to binary.
     *
     * If the object is null, empty ( zero ) array of given length is returned,
     * result of toBytes() method called on the given object is returned
     * otherwise.
     *
     * @param o      Object to serialize to binary
     * @param length Length of the empty ( zero ) byte array, if null
     * @return Binary
     */
    public static byte[] toBytes(BinarySerializable o, int length)
    {
        return o == null ? new byte[length] : o.toBytes();
    }

    /**
     * Null-safe converter from {@link BinarySerializable} object to binary.
     *
     * If the object is null, {0x00} is returned, result of toBytes() method
     * called on the given object is returned otherwise.
     *
     * @param o Object to serialize to binary
     * @return Binary
     */
    public static byte[] toBytes(BinarySerializable o)
    {
        return toBytes(o, 1);
    }

    /**
     * Serializes the given map of binary serializable objects to binary
     * Notice: strings are always encoded with the length byte!
     *
     * Supported types: Byte[], Byte, Boolean, String, Short, Integer, Long and
     * classes that implement {@link BinarySerializable} interface.
     *
     * @param map The map to serialize
     * @return Binary
     */
    public static byte[] toBytes(Map<?, ?> map)
    {
        if (map == null || map.size() == 0) return new byte[1]; // zero length map

        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(toVarIntBytes(map.size()));
        map.forEach((key, value) -> {
            bytes.append(convertObjectToBinary(key));
            bytes.append(convertObjectToBinary(value));
        });
        return bytes.getBytes();
    }

    /**
     * Serializes the given list of binary serializable object to binary
     * Notice: strings are always encoded with the length byte!
     *
     * Supported types: Byte[], Byte, Boolean, String, Short, Integer, Long and
     * classes that implement {@link BinarySerializable} interface.
     *
     * @param list The list to serialize
     * @return Binary
     */
    public static byte[] toBytes(List<?> list)
    {
        if (list == null || list.size() == 0) return new byte[1]; // zero-length list

        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(toVarIntBytes(list.size()));
        list.forEach(value -> bytes.append(convertObjectToBinary(value)));
        return bytes.getBytes();
    }

    private static byte[] convertObjectToBinary(Object o)
    {
        if (o == null) return new byte[0];

        if (o instanceof Byte[]) return toPrimitiveBytes((Byte[]) o);
        if (o instanceof Byte) return new byte[]{(Byte) o};
        if (o instanceof Boolean) return new byte[]{toByte((Boolean) o)};
        if (o instanceof BinarySerializable) return toBytes((BinarySerializable) o);
        if (o instanceof String) return toBytes((String) o, true);
        if (o instanceof Short) return toBytes((Short) o);
        if (o instanceof Integer) return toBytes((Integer) o);
        if (o instanceof Long) return toBytes((Long) o);

        throw new IllegalArgumentException("Unsupported type, can't convert to binary");
    }

    public static byte[] toPrimitiveBytes(Byte[] bytes)
    {
        byte[] primitiveBytes = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            primitiveBytes[i] = bytes[i];
        return primitiveBytes;
    }

    public static Byte[] toObjectBytes(byte[] array) {
        final Byte[] objectBytes = new Byte[array.length];
        for (int i = 0; i < array.length; i++) {
            objectBytes[i] = array[i];
        }
        return objectBytes;
    }
}
