/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import com.sophiatx.lwsphtxbcl.chain.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class ISO8601
{
    public static final TimeZone tZone = TimeZone.getTimeZone(Configuration.TIME_ZONE);
    public static final SimpleDateFormat df = new SimpleDateFormat(Configuration.DATE_FORMAT);

    static {
        df.setTimeZone(tZone);
    }

    public static LocalDateTime toDate(String dateString)
    throws ParseException
    {
        return df.parse(dateString)
                .toInstant()
                .atZone(Configuration.ZONE_ID)
                .toLocalDateTime();
    }

    public static String toString(LocalDateTime date)
    {
        return df.format(date);
    }

    public static long toEpoch(LocalDateTime date)
    {
        return date.atZone(Configuration.ZONE_ID).toEpochSecond();
    }
}
