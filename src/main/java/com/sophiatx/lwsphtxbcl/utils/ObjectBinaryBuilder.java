/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class ObjectBinaryBuilder extends ByteArrayBuilder
{
    public ObjectBinaryBuilder append(Byte[] value)
    {
        append(BinUtils.toPrimitiveBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(Byte value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(Boolean value)
    {
        append(BinUtils.toByte(value));
        return this;
    }

    public ObjectBinaryBuilder append(BinarySerializable value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(String value)
    {
        append(BinUtils.toBytes(value, true));
        return this;
    }

    public ObjectBinaryBuilder append(Short value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(Integer value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(Long value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(Map<?, ?> value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(List<?> value)
    {
        append(BinUtils.toBytes(value));
        return this;
    }

    public ObjectBinaryBuilder append(LocalDateTime value)
    {
        append(BinUtils.toBytes((int) ISO8601.toEpoch(value)));
        return this;
    }
}
