/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.conversion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Flat map is a non-standard JSON structure used by the blockchain.
 * It represents maps by using arrays instead of objects and looks like this:
 *
 * [
 *     [key, value],
 *     [key, value],
 *     ...
 * ]
 */
public class FlatMapDeserializer extends JsonDeserializer<Map<Object, Object>>
        implements ContextualDeserializer
{

    private Class<?> keyClass;
    private Class<?> contentClass;

    @Override
    public Map<Object, Object> deserialize(JsonParser p, DeserializationContext ctxt)
    throws IOException
    {
        return this.deserialize(p, ctxt, new HashMap<>());
    }

    @Override
    public Map<Object, Object> deserialize(
            JsonParser p, DeserializationContext ctxt, Map<Object, Object> intoValue
    )
    throws IOException
    {
        JsonNode node = p.readValueAsTree();
        ObjectCodec codec = p.getCodec();

        if (!node.isArray())
            throw new IOException("FlatMap must be an array");

        for (JsonNode entry : node) {
            JsonNode keyNode = entry.get(0);
            JsonNode valueNode = entry.get(1);

            if (keyNode == null)
                throw new IOException("Missing key in flat map");
            if (valueNode == null)
                throw new IOException("Missing value in flat map");

            intoValue.put(
                    keyNode.traverse(codec).readValueAs(this.keyClass),
                    valueNode.traverse(codec).readValueAs(this.contentClass)
            );
        }

        return intoValue;
    }

    @Override
    public JsonDeserializer<?> createContextual(
            DeserializationContext ctxt,
            BeanProperty property
    )
    {
        JsonDeserialize jsonDeserialize = property.getAnnotation(JsonDeserialize.class);
        this.keyClass = jsonDeserialize.keyAs();
        this.contentClass = jsonDeserialize.contentAs();
        return this;
    }
}
