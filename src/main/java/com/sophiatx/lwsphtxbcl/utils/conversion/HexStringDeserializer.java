/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.conversion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;

import java.io.IOException;

public class HexStringDeserializer extends JsonDeserializer<Byte[]>
{
    @Override
    public Byte[] deserialize(JsonParser p, DeserializationContext ctxt)
    throws IOException, JsonProcessingException
    {
        JsonNode node = p.readValueAsTree();
        if (!node.isTextual())
            throw new IOException("HEX string must be textual");

        return BinUtils.toObjectBytes(Hex.toBytes(node.asText()));
    }
}
