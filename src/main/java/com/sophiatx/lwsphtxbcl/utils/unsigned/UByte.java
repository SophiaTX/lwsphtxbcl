/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.unsigned;

import com.sophiatx.lwsphtxbcl.utils.BinUtils;

import java.math.BigInteger;

public final class UByte extends UNumber implements Comparable<UByte>
{
    public static final short MIN_VALUE = 0x00;
    public static final short MAX_VALUE = 0xff;
    public static final UByte MIN = valueOf(0x00);
    public static final UByte MAX = valueOf(0xff);

    private final short value;

    public static UByte valueOf(String value)
    throws NumberFormatException
    {
        return new UByte(value);
    }

    public static UByte valueOf(byte value)
    {
        return new UByte(value);
    }

    public static UByte valueOf(short value)
    throws NumberFormatException
    {
        return new UByte(value);
    }

    public static UByte valueOf(int value)
    throws NumberFormatException
    {
        return new UByte(value);
    }

    public static UByte valueOf(long value)
    throws NumberFormatException
    {
        return new UByte(value);
    }

    private UByte(long value)
    throws NumberFormatException
    {
        this.value = (short) rangeCheck(value);
    }

    private UByte(int value)
    throws NumberFormatException
    {
        this.value = (short) rangeCheck(value);
    }

    private UByte(short value)
    throws NumberFormatException
    {
        this.value = (short) rangeCheck(value);
    }

    private UByte(byte value)
    {
        this.value = (short) (value & MAX_VALUE);
    }

    private UByte(String value)
    throws NumberFormatException
    {
        this(Short.parseShort(value));
    }

    @Override
    public int intValue()
    {
        return value;
    }

    @Override
    public long longValue()
    {
        return value;
    }

    @Override
    public float floatValue()
    {
        return value;
    }

    @Override
    public double doubleValue()
    {
        return value;
    }

    @Override
    public int hashCode()
    {
        return Short.valueOf(value).hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o instanceof UByte)
            return value == ((UByte) o).value;
        return false;
    }

    @Override
    public String toString()
    {
        return Short.valueOf(value).toString();
    }

    @Override
    public byte[] toBytes()
    {
        return BinUtils.toBytes((byte) value);
    }

    @Override
    public int compareTo(UByte o)
    {
        return Short.compare(value, o.value);
    }

    @Override
    public BigInteger toBigInteger()
    {
        return BigInteger.valueOf(value);
    }
}
