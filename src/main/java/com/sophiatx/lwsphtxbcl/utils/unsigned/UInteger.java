/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.unsigned;

import com.sophiatx.lwsphtxbcl.utils.BinUtils;

import java.math.BigInteger;

public final class UInteger extends UNumber implements Comparable<UInteger>
{
    public static final long MIN_VALUE = 0x00000000;
    public static final long MAX_VALUE = 0xffffffffL;
    public static final UInteger MIN = valueOf(MIN_VALUE);
    public static final UInteger MAX = valueOf(MAX_VALUE);

    private final long value;

    public static UInteger valueOf(String value)
    throws NumberFormatException
    {
        return new UInteger(value);
    }

    public static UInteger valueOf(int value)
    {
        return new UInteger(value);
    }

    public static UInteger valueOf(long value)
    throws NumberFormatException
    {
        return new UInteger(value);
    }

    private UInteger(long value)
    throws NumberFormatException
    {
        this.value = rangeCheck(value);
    }

    private UInteger(int value)
    {
        this.value = value & MAX_VALUE;
    }

    private UInteger(String value)
    throws NumberFormatException
    {
        this(Long.parseLong(value));
    }

    @Override
    public int intValue()
    {
        return (int) value;
    }

    @Override
    public long longValue()
    {
        return value;
    }

    @Override
    public float floatValue()
    {
        return value;
    }

    @Override
    public double doubleValue()
    {
        return value;
    }

    @Override
    public BigInteger toBigInteger()
    {
        return BigInteger.valueOf(value);
    }

    @Override
    public int hashCode()
    {
        return Long.valueOf(value).hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o instanceof UInteger)
            return value == ((UInteger) o).value;
        return false;
    }

    @Override
    public String toString()
    {
        return Long.valueOf(value).toString();
    }

    @Override
    public byte[] toBytes()
    {
        return BinUtils.toBytes(intValue());
    }

    @Override
    public int compareTo(UInteger o)
    {
        return Long.compare(value, o.value);
    }
}
