/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.unsigned;

import com.sophiatx.lwsphtxbcl.utils.BinUtils;

import java.math.BigInteger;

public final class UShort extends UNumber implements Comparable<UShort>
{
    public static final int MIN_VALUE = 0x0000;
    public static final int MAX_VALUE = 0xffff;
    public static final UShort MIN = valueOf(MIN_VALUE);
    public static final UShort MAX = valueOf(MAX_VALUE);

    private final int value;

    public static UShort valueOf(String value)
    throws NumberFormatException
    {
        return new UShort(value);
    }

    public static UShort valueOf(short value)
    {
        return new UShort(value);
    }

    public static UShort valueOf(int value)
    throws NumberFormatException
    {
        return new UShort(value);
    }

    private UShort(int value)
    throws NumberFormatException
    {
        this.value = (int) rangeCheck(value);
    }

    private UShort(short value)
    {
        this.value = value & MAX_VALUE;
    }

    private UShort(String value)
    throws NumberFormatException
    {
        this(Integer.parseInt(value));
    }

    @Override
    public int intValue()
    {
        return value;
    }

    @Override
    public long longValue()
    {
        return value;
    }

    @Override
    public float floatValue()
    {
        return value;
    }

    @Override
    public double doubleValue()
    {
        return value;
    }

    @Override
    public BigInteger toBigInteger()
    {
        return BigInteger.valueOf(value);
    }

    @Override
    public int hashCode()
    {
        return Integer.valueOf(value).hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o instanceof UShort)
            return value == ((UShort) o).value;
        return false;
    }

    @Override
    public String toString()
    {
        return Integer.valueOf(value).toString();
    }

    @Override
    public byte[] toBytes()
    {
        return BinUtils.toBytes(shortValue());
    }

    @Override
    public int compareTo(UShort o)
    {
        return Integer.compare(value, o.value);
    }
}
