/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class AssetTests
{
    private static ObjectMapper mapper;

    private static Asset assetSphtx;
    private static final byte[] sphtxBytes = Hex.toBytes("f10d2299000000005350485458000000");
    private static final String sphtxStr = "2569.145841 SPHTX";

    private static Asset assetEur;
    private static final byte[] eurBytes = Hex.toBytes("004b7a04000000004555520000000000");
    private static final String eurStr = "75.123456 EUR";

    @BeforeClass
    public static void createAsset()
    {
        mapper = new ObjectMapper();
        assetSphtx = new Asset("2569.145841", AssetSymbol.SPHTX);
        assetEur = new Asset("75.123456", AssetSymbol.EUR);
    }

    @Test
    public void testConstructFromString()
    {
        assertEquals(assetSphtx, new Asset(sphtxStr));
        assertEquals(assetEur, new Asset(eurStr));
    }

    @Test
    public void testToString()
    {
        assertEquals(sphtxStr, assetSphtx.toString());
        assertEquals(eurStr, assetEur.toString());
    }

    @Test
    public void testToBin()
    {
        assertArrayEquals(sphtxBytes, assetSphtx.toBytes());
        assertArrayEquals(eurBytes, assetEur.toBytes());
    }

    @Test
    public void testJsonSerialization()
    throws JsonProcessingException
    {
        assertEquals("\"" + sphtxStr + "\"", mapper.writeValueAsString(assetSphtx));
        assertEquals("\"" + eurStr + "\"", mapper.writeValueAsString(assetEur));
    }

    @Test
    public void testJsonDeserialization()
    throws IOException
    {
        assertEquals(assetSphtx, mapper.readValue("\"" + sphtxStr + "\"", Asset.class));
        assertEquals(assetEur, mapper.readValue("\"" + eurStr + "\"", Asset.class));
    }
}
