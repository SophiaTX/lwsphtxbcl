/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class ChainPropertiesTests
{
    private static final ObjectMapper mapper;
    private static final String json = "{\"account_creation_fee\":\"1.000000 SPHTX\",\"maximum_block_size\":16384,\"price_feeds\":[[\"USD\",{\"base\":\"1.000000 SPHTX\",\"quote\":\"10.000000 USD\"}]]}";
    private static ChainProperties props;

    static {
        mapper = new ObjectMapper();
    }

    @BeforeClass
    public static void createChainProps()
    {
        HashMap<AssetSymbol, ConversionRate> priceFeeds
                = new HashMap<>();
        priceFeeds.put(AssetSymbol.USD, new ConversionRate(
                new Asset("1", AssetSymbol.SPHTX),
                new Asset("10", AssetSymbol.USD)
        ));

        props = new ChainProperties(
                new Asset("1", AssetSymbol.SPHTX),
                UInteger.valueOf(16384),
                priceFeeds
        );
    }

    @Test
    public void testSerializeChainProperties()
    throws JsonProcessingException
    {
        assertEquals(json, mapper.writeValueAsString(props));
    }

    @Test
    public void testDeserializeChainProperties()
    throws IOException
    {
        assertEquals(props, mapper.readValue(json, ChainProperties.class));
    }
}
