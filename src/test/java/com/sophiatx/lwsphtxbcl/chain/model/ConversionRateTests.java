/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConversionRateTests
{
    // example rate
    private static final ConversionRate RATE = new ConversionRate(
            new Asset("1", AssetSymbol.SPHTX),
            new Asset("5.23", AssetSymbol.EUR)
    );

    // example pair, should match and be convertible
    private static final Asset EUR = new Asset("9.500002", AssetSymbol.EUR);
    private static final Asset SPHTX = new Asset("1.816444", AssetSymbol.SPHTX);

    @Test
    public void testConversionToBase()
    {
        assertEquals(SPHTX, RATE.convert(EUR));
    }

    @Test
    public void testConversionFromBase()
    {
        assertEquals(EUR, RATE.convert(SPHTX));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertInvalidAsset()
    {
        RATE.convert(new Asset("10", AssetSymbol.GBP));
    }
}
