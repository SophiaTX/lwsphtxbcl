/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.Samples;
import com.sophiatx.lwsphtxbcl.chain.Chains;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AccountWitnessVoteOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.TransferOperation;
import com.sophiatx.lwsphtxbcl.crypto.CompactSignature;
import com.sophiatx.lwsphtxbcl.crypto.Sha256Hash;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;

import static com.sophiatx.lwsphtxbcl.Samples.FEE_CONVERSION_RATE;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TransactionTests
{
    private static final ObjectMapper mapper = new ObjectMapper();

    private static Transaction tx;
    private static final String txJson = "{\"ref_block_num\":32441,\"ref_block_prefix\":2784635841,\"expiration\":\"2018-09-25T13:05:12\",\"operations\":[[\"account_witness_vote\",{\"fee\":\"0.000000 SPHTX\",\"account\":\"pato\",\"witness\":\"abc\",\"approve\":false}],[\"transfer\",{\"fee\":\"0.010000 SPHTX\",\"from\":\"pato\",\"to\":\"matus\",\"amount\":\"1.000000 SPHTX\",\"memo\":\"Testing serialized TX\"}]],\"extensions\":[],\"signatures\":[]}";
    private static final byte[] txBin = Hex.toBytes("B97EC12BFAA58832AA5B020900000000000000005350485458000000047061746F03616263000010270000000000005350485458000000047061746F056D6174757340420F000000000053504854580000001554657374696E672073657269616C697A656420545800");
    private static final Sha256Hash txDigest = Sha256Hash.wrap("594345fe402a768ab0054cdac0b653d56076fa10b71a95653ddc242aaf408bbd");
    private static final CompactSignature sign = new CompactSignature("20ef6a3c3100e958a1d02e4980657a5ff06e08c4bd2a4cdafe566467316031cccd6f676a80ef387f554832023a85a424fa2aa40564d6fe74e58e29df4b1ecf5ed5");

    @BeforeClass
    public static void createTransaction()
    {
        tx = new Transaction(UShort.valueOf(32441), UInteger.valueOf(2784635841L))
                .expireAt(LocalDateTime.parse("2018-09-25T13:05:12"))
                .addOperation(new AccountWitnessVoteOperation(
                        "pato",
                        "abc",
                        false
                ))
                .addOperation(new TransferOperation(
                        "pato",
                        "matus",
                        new Asset("1", AssetSymbol.SPHTX),
                        "Testing serialized TX"
                ))
                .calculateFees(FEE_CONVERSION_RATE);
    }

    @Test
    public void testSignTransaction()
    {
        SignedTransaction signedTx = tx.sign(Samples.KEY_PAIR, Chains.DEV_NET);
        assertEquals(sign, signedTx.getSignatures().get(0));
    }

    @Test
    public void testGetDigest()
    {
        assertEquals(txDigest, tx.getDigest(Chains.DEV_NET));
    }

    @Test
    public void testSerializeBinary()
    {
        assertArrayEquals(txBin, tx.toBytes());
    }

    @Test
    public void testSerializeJson()
    throws JsonProcessingException
    {
        assertEquals(txJson, mapper.writeValueAsString(tx));
    }

    @Test
    public void testDeserializeJson()
    throws IOException
    {
        Transaction trx = mapper.readValue(txJson, Transaction.class);
        assertEquals(tx, trx);
    }
}
