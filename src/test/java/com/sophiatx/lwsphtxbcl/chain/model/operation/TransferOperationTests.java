/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static com.sophiatx.lwsphtxbcl.Samples.FEE_CONVERSION_RATE;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TransferOperationTests
{
    private static TransferOperation op;
    private static final String json = "[\"transfer\",{\"fee\":\"0.010000 SPHTX\",\"from\":\"pato\",\"to\":\"matus\",\"amount\":\"1.000000 SPHTX\",\"memo\":\"Testing serialized TX\"}]";
    private static final byte[] bin = Hex.toBytes("10270000000000005350485458000000047061746f056d6174757340420f000000000053504854580000001554657374696e672073657269616c697a6564205458");

    @BeforeClass
    public static void createOperation()
    {
        op = new TransferOperation(
                "pato",
                "matus",
                new Asset("1", AssetSymbol.SPHTX),
                "Testing serialized TX"
        );
        op.calculateFee(FEE_CONVERSION_RATE);
    }

    @Test
    public void testDeserializeJson()
    throws IOException
    {
        assertEquals(op, new ObjectMapper().readValue(json, TransferOperation.class));
    }

    @Test
    public void testSerializeJson()
    throws JsonProcessingException
    {
        assertEquals(json, new ObjectMapper().writeValueAsString(op));
    }

    @Test
    public void testSerializeBinary()
    {
        assertArrayEquals(bin, op.toBytes());
    }
}
