/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.comm.api.database.request.GetCurrentPriceFeedRequest;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.utils.ObjectDumper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DatabaseApiTests
{
    private static Connector connector;

    @BeforeClass
    public static void createConnector()
    throws UnknownHostException
    {
        connector = new Connector(
                (Inet4Address) InetAddress.getByName("socket1.sophiatx.com"),
                9193
        );
    }

    @Test
    public void testGetCurrentPriceFeed()
    throws CommunicationException
    {
        ConversionRate rate = connector.execute(new GetCurrentPriceFeedRequest(AssetSymbol.USD));
        ObjectDumper.print(rate);
    }
}
