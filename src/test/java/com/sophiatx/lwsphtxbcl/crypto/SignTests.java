/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import org.junit.Test;

import java.math.BigInteger;

import static com.sophiatx.lwsphtxbcl.Samples.*;
import static org.junit.Assert.assertEquals;

public class SignTests
{
    @Test
    public void testSignDigest()
    {
        CompactSignature signature = Sign.signDigest(MESSAGE_SHA256, KEY_PAIR);
        assertEquals(SIGNATURE, signature);
    }

    @Test
    public void testRecoverPublicKeyFromSignature()
    {
        BigInteger key = Sign.recoverPublicKey(SIGNATURE, MESSAGE_SHA256);
        assertEquals(PUBLIC_KEY, key);
    }
}
