/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.encoding;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Base58Tests
{
    private static final byte[] message = "This is a test message!".getBytes();
    private static final String encoded = "2k7m8YHnAT7dp9TqXA2TBsmXvZnZKaqn";

    @Test
    public void testBase58Encoding()
    {
        assertEquals(encoded, Base58.encode(message));
    }

    @Test
    public void testBase58Decoding()
    {
        assertArrayEquals(message, Base58.decode(encoded));
    }
}
