/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.encoding;

import org.junit.Test;

import java.math.BigInteger;

import static com.sophiatx.lwsphtxbcl.Samples.*;
import static org.junit.Assert.assertEquals;

public class KeyEncoderTests
{
    @Test
    public void testEncodeWif()
    {
        String encoded = KeyEncoder.encodeWif(PRIVATE_KEY);
        assertEquals(PRIVATE_KEY_STRING, encoded);
    }

    @Test
    public void testDecodeWif()
    {
        BigInteger decoded = KeyEncoder.decodeWif(PRIVATE_KEY_STRING);
        assertEquals(PRIVATE_KEY, decoded);
    }

    @Test
    public void testEncodePublicKey()
    {
        String encoded = KeyEncoder.encodePublicKey(PUBLIC_KEY);
        assertEquals(PUBLIC_KEY_STRING, encoded);
    }

    @Test
    public void testDecodePublicKey()
    {
        BigInteger decoded = KeyEncoder.decodePublicKey(PUBLIC_KEY_STRING);
        assertEquals(PUBLIC_KEY, decoded);
    }
}
