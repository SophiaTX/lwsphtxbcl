/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertArrayEquals;

public class BinUtilsTests
{
    private byte[] strBinWithLength = new byte[]{0x06, 0x70, 0x69, 0x73, 0x74, 0x6f, 0x6e};
    private byte[] strBin = new byte[]{0x70, 0x69, 0x73, 0x74, 0x6f, 0x6e};
    private String strVal = "piston";

    @Test
    public void testToBytesPadded()
    {
        byte[] result = BinUtils.toBytesPadded(new BigInteger("127"), 3);
        assertArrayEquals(new byte[]{0x00, 0x00, 0x7F}, result);
    }

    @Test
    public void testByteConcat()
    {
        byte[] ary1 = new byte[]{0x0A, 0x1B};
        byte[] ary2 = new byte[]{0x2C, 0x3D};
        byte[] result = BinUtils.concat(ary1, ary2);
        assertArrayEquals(new byte[]{0x0A, 0x1B, 0x2C, 0x3D}, result);
    }

    @Test
    public void testByteAdd()
    {
        byte[] ary = new byte[]{0x0A, 0x1B};
        byte b = 0x2C;
        byte[] result = BinUtils.add(ary, b);
        byte[] expected = new byte[]{0x0A, 0x1B, 0x2C};
        assertArrayEquals(expected, result);
    }

    @Test
    public void testStringToBinWithLength()
    {
        assertArrayEquals(strBinWithLength, BinUtils.toBytes(strVal, true));
    }

    @Test
    public void testStringToBin()
    {
        assertArrayEquals(strBin, BinUtils.toBytes(strVal, false));
        assertArrayEquals(strBin, BinUtils.toBytes(strVal));
    }

    // TODO: add bin converter tests
}
